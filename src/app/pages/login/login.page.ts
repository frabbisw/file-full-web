import { Constants } from './../../entity/constants';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { User } from '../../entity/user';
import { CacheService } from 'ionic-cache';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: string;
  password: string;
  isHidden = true;
  loginEnabled = true;
  defaultUser: User;
  constructor(public httpClient: HttpClient, public navCtrl: NavController, private platform: Platform,
              public toastController: ToastController, public cache: CacheService) { }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.cache.getItem('user_data').then(
        data => {
          if (data != null) {
            console.log('offline user data found');
            console.log(data);
            // this.navCtrl.navigateRoot('menu/home');
            this.navCtrl.navigateRoot('menu/sync');
          } else {
            console.log('offline user data not found');
          }
        }
      ).catch(() => console.log('offline data not found'));
    }).catch(() => {
      (navigator as any).app.exitApp();
    });
  }

  async presentToast(messageText, timeout) {
    const toast = await this.toastController.create({
      message: messageText,
      duration: timeout
    });
    toast.present();
  }

  handle_login(response) {
    console.log(response.status);
    if (response.status === 1) {
      try {
           this.defaultUser = new User(response.data.user.email, response.data.user.name, response.access_token);
           console.log(this.defaultUser.fullName);
           console.log(this.defaultUser.email);
           console.log(this.defaultUser.apiToken);

           try {
            this.cache.saveItem('user_data', this.defaultUser).
              then(() => {
                this.navCtrl.navigateRoot('menu/sync');
                this.isHidden = true;
                this.loginEnabled = true;
              }
              );
          } catch {
            this.presentToast('error while parsing files', 1000);
            this.isHidden = true;
            this.loginEnabled = true;
          }
      } catch {
        this.presentToast('error parsing', 1000);
        this.isHidden = true;
        this.loginEnabled = true;
      }
    } else {
      this.presentToast('Wrong email or password', 1000);
      this.isHidden = true;
      this.loginEnabled = true;
    }
  }

  handle_error(error) {
    console.log(JSON.stringify(error));
    this.isHidden = true;
    this.loginEnabled = true;

    // this.presentToast(JSON.stringify(error), 1000);

    if (error.status === 401) {
      console.log('login failed');
      this.presentToast('Wrong email or password', 1000);
    } else {
      if (error.status === 422) {
        console.log('login failed');
        // this.presentToast('Invalid Data', 1000);
        try {
          this.presentToast(error.error.errors.email, 1000);
        } catch {
          this.presentToast('Invalid Input Data', 1000);
        }
      } else {
        this.presentToast('Network Error', 1000);
      }
    }
  }

  login() {
    this.presentToast('Logging in...', 1000);

    this.isHidden = false;
    this.loginEnabled = false;

    const postData = {
      email: this.email,
      password: this.password,
      api_key: Constants.API_KEY,
    };
    const httpHeaders = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const options = {
      headers: httpHeaders
    };

    // (url, postData, httpHeaders).then(data => this.handle_login(data) ).catch(error => this.handleError(error) );

    const url = Constants.BASE_URL + 'api/auth/login';
    this.httpClient.post(url, postData, options).subscribe(data => {this.handle_login(data); }, err => this.handle_error(err) );
  }
}

// frduiit@gmail.com 1111
