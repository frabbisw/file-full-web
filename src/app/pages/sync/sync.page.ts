import { Constants } from './../../entity/constants';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { NavController, ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { User } from 'src/app/entity/user';
import { CacheService } from 'ionic-cache';

@Component({
  selector: 'app-sync',
  templateUrl: './sync.page.html',
  styleUrls: ['./sync.page.scss'],
})
export class SyncPage implements OnInit {
  defaultUser: User;
  isHidden = true;
  loginEnabled = true;

  constructor(public httpClient: HttpClient, public navCtrl: NavController, public cache: CacheService,
              public toastController: ToastController, public alertController: AlertController) { }

  ngOnInit() {
    this.presentToast('syncing...', 1000);
    this.isHidden = false;
    this.loginEnabled = false;
    this.cache.getItem('user_data').
    then(user => {
      console.log('sync ngOnInit');
      this.defaultUser = user;
      this.retrieveFiles(this.defaultUser.apiToken);
    }).
    catch(() => {
      console.log('error retrieving user in sync');
    });
  }
  retrieveFiles(apiToken) {
    console.log('sync retrieveFiles');
    console.log(apiToken);
    this.presentToast('Retrieving Files...', 1000);

    const httpHeaders = new HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    });
    const apiKey = Constants.API_KEY;

    const options = {
      headers: httpHeaders
    };

    const url = Constants.BASE_URL + 'api/user/files?api_key=' + apiKey + '&api_token=' + apiToken;

    this.httpClient.get(url, options).subscribe(data => {this.file_success(data); }, err => this.handle_error(err) );
  }

  handle_error(error) {
    this.isHidden = true;
    this.loginEnabled = true;

    console.log(error);

    if (error.status === 401) {
      console.log('login failed');
      this.presentToast('Session Expired! Login again', 1000);
      this.navCtrl.navigateRoot('menu/home');
    } else {
      if (error.status === 422) {
        console.log('login failed');
        this.presentToast('Session Expired! Login again', 1000);
        this.navCtrl.navigateRoot('menu/home');
      }
      this.presentToast('Network Error', 1000);
      this.navCtrl.navigateRoot('menu/home');
    }
  }

  dateAsYYYYMMDDHHNNSS(date): string {
    return date.getFullYear()
              + '-' + this.leftpad(date.getMonth() + 1, 2)
              + '-' + this.leftpad(date.getDate(), 2)
              + ' ' + this.leftpad(date.getHours(), 2)
              + ':' + this.leftpad(date.getMinutes(), 2)
              + ':' + this.leftpad(date.getSeconds(), 2);
  }

  leftpad(val, resultLength = 2, leftpadChar = '0'): string {
    return (String(leftpadChar).repeat(resultLength)
          + String(val)).slice(String(val).length);
  }
  file_success(response) {
    const dateTime = this.dateAsYYYYMMDDHHNNSS(new Date());
    console.log(dateTime);
    try {
      this.cache.saveItem('files_data', response ).
        then(() => this.cache.saveItem('saving_time', dateTime).then(() => {
          this.navCtrl.navigateRoot('menu/home');
          this.isHidden = true;
          this.loginEnabled = true;
        }).catch(error => {
          this.isHidden = true;
          this.loginEnabled = true;
          this.presentToast('error while saving files', 1000);
        }));
    } catch {
      this.presentToast('error while parsing files', 1000);
      this.isHidden = true;
      this.loginEnabled = true;
    }
  }

  handle_login(response) {
    if (response.status === 200) {
      try {
        // this.presentToast(response.data, 5000);
        const data = JSON.parse(response.data);

        this.defaultUser = new User(data.data.user.email, data.data.user.name, data.access_token);
        // this.userService.setUser(user);

        this.retrieveFiles(this.defaultUser.apiToken);
      } catch {
        this.presentToast('error parsing', 1000);
        this.isHidden = true;
        this.loginEnabled = true;
      }
    } else if (response.status === 200) {
      this.presentToast('Wrong email or password', 1000);
      this.isHidden = true;
      this.loginEnabled = true;
    }
  }
  async presentToast(messageText, timeout) {
    const toast = await this.toastController.create({
      message: messageText,
      duration: timeout
    });
    toast.present();
  }

}
