import { Constants } from './../../entity/constants';
import { Component, OnInit } from '@angular/core';
import { NavController, Platform, ToastController, AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { User } from '../../entity/user';
import { IFolder } from '../../entity/folder';
import { IFile } from '../../entity/file';
import { CacheService } from 'ionic-cache';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  user: User;
  fileMap: Map<any, any>;
  folderMap: Map<any, any>;
  folders = [];
  files = [];
  currentId = 0;
  pageTitle: string;
  currentFolder: IFolder;
  syncDate: Date;
  timeInterval = 7;

  constructor(public platform: Platform, public navCtrl: NavController,
              public alertController: AlertController, public cache: CacheService,
              public toastController: ToastController, public httpClient: HttpClient) { }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.platformReady();
    });
  }

  platformReady() {
    this.cache.getItem('time_interval').then(ti => {
      this.timeInterval = ti;
      this.decorate();
    }).catch(() => {
      this.timeInterval = Constants.TIME_INTERVAL;
      this.decorate();
    });
  }
  decorate() {
    this.cache.getItem('files_data')
    .then(
      data => {
        console.log(data);
        this.cache.getItem('user_data').then(user => {
          this.user = user;

          const fileMap = new Map();
          const folderMap = new Map();
          data.folders.forEach(element => {
            folderMap.set(element.id, element);
          });
          data.files.forEach(element => {
            fileMap.set(element.id, element);
          });

          this.fileMap = fileMap;
          this.folderMap = folderMap;

          console.log(this.user);
          console.log(this.fileMap);
          console.log(this.folderMap);

          this.prepare_folder(this.currentId);
        }).catch(() => {
          this.presentToast('error while retrieving user data', 1000);
        });
      }
    ).catch(() => {
      this.presentToast('error while retrieving files', 1000);
    });
  }

  async presentToast(messageText, timeout) {
    const toast = await this.toastController.create({
      message: messageText,
      duration: timeout
    });
    toast.present();
  }

  prepare_folder(parentId: number) {
    this.currentId = parentId;
    if (this.currentId === 0) {
      this.pageTitle = 'Home';
    } else {
      this.pageTitle = this.folderMap.get(this.currentId).name;
    }

    this.folders = [];
    this.files = [];

    this.folderMap.forEach((value: any, key: number) => {
      if (value.parent_id === parentId) {
        const folder = new IFolder(value.id, value.name, value.parent_id,
          value.created_at, value.updated_at);
        this.folders.push(folder);
      }
    });
    this.fileMap.forEach((value: any, key: number) => {

      if (value.folder_id === parentId) {
        const file = new IFile(value.id, value.folder_id, value.name, value.path, value.size,
          value.mime, value.ext, value.created_at, value.updated_at);
        this.files.push(file);
      }
    });
  }

  go_upward() {
    if (this.currentId === 0) { return; }
    this.currentId = this.folderMap.get(this.currentId).parent_id;
    this.prepare_folder(this.currentId);
  }
  download(data) {
    console.log(data.download_url);
    window.open(data.download_url, '_system');
  }
  async dclick(file) {
    const alert = await this.alertController.create({
      header: 'Download this file?',
      message: 'Tap <strong>Ok</strong> to Download',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.presentToast('Generating Download URL...', 1000);

            const httpHeaders = new HttpHeaders({
              Accept: 'application/json',
              'Content-Type': 'application/json'
            });
            const options = {
              headers: httpHeaders
            };
            const params = {
              api_key: Constants.API_KEY,
              api_token: this.user.apiToken
            };

            const apiKey = Constants.API_KEY;
            const apiToken = this.user.apiToken;

            const url = Constants.BASE_URL + 'api/user/files/' +
            Number(file.id).toString() + '/download?api_key=' + apiKey + '&api_token=' + apiToken;

            // console.log(url);

            this.httpClient.get(url, options).subscribe(data => {this.download(data); },
            err => {this.presentToast('error getting durl', 1000); });
          }
        }
      ]
    });

    await alert.present();
  }
}
