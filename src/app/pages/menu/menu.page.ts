import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, Platform } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { CacheService } from 'ionic-cache';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  title = 'Mr. X';
  constructor(public navCtrl: NavController, private platform: Platform, public cache: CacheService,
              public toastController: ToastController, public alertController: AlertController) { }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.cache.getItem('user_data').then(user => {
        this.title = user.fullName;
      }).catch(() => {
        this.presentToast('error while retrieving user data', 1000);
      });
    });
  }

  async presentToast(messageText, timeout) {
    const toast = await this.toastController.create({
      message: messageText,
      duration: timeout
    });
    toast.present();
  }

  logout() {
    this.cache.saveItem('user_data', null).then(
      () => {
        this.presentToast('Logging out...', 1000);
        this.navCtrl.navigateRoot('login');
      }
    ).catch();
  }
  sync() {
    this.navCtrl.navigateRoot('menu/sync');
    // this.presentAlertConfirm();
  }
  exit() {
    this.confirmExit();
  }
  async confirmExit() {
    const alert = await this.alertController.create({
      header: 'Exit?',
      message: 'Tap <strong>Confirm</strong> to Exit',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
        , {
          text: 'Confirm',
          handler: () => {
            console.log('Confirm Open');
            (navigator as any).app.exitApp();
          }
        }
      ]
    });

    await alert.present();
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Login again to sync!',
      message: 'Tap <strong>Confirm</strong> to sync',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Login',
          handler: () => {
            console.log('Confirm Okay');
            this.logout();
          }
        }
      ]
    });

    await alert.present();
  }
}
