import { NewfilesPageModule } from './../newfiles/newfiles.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'modified',
        loadChildren: () => import('../modified/modified.module').then( m => m.ModifiedPageModule)
      },
      {
        path: 'newfiles',
        loadChildren: () => import('../newfiles/newfiles.module').then( m => m.NewfilesPageModule)
      },
      {
        path: 'sync',
        loadChildren: () => import('../sync/sync.module').then( m => m.SyncPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
