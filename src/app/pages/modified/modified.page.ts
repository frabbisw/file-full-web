import { Constants } from './../../entity/constants';
import { Component, OnInit } from '@angular/core';
import { NavController, Platform, ToastController, AlertController } from '@ionic/angular';

import { User } from '../../entity/user';
import { IFile } from '../../entity/file';

import { CacheService } from 'ionic-cache';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-modified',
  templateUrl: './modified.page.html',
  styleUrls: ['./modified.page.scss'],
})
export class ModifiedPage implements OnInit {
  user: User;
  fileMap: Map<any, any>;
  files = [];
  timeInterval = 7;
  syncDate: Date;

  constructor(public platform: Platform, public httpClient: HttpClient,
              public navCtrl: NavController, public alertController: AlertController, public cache: CacheService,
              public toastController: ToastController) { }

  ngOnInit() {
    this.cache.getItem('time_interval').then(ti => {
      this.timeInterval = ti;
      this.cache.getItem('user_data').then(user => {
        this.user = user;
        this.makeListOfFiles();
      }).catch(() => {
        this.presentToast('error while retrieving user data', 1000);
      });
    }).catch(() => {
      this.timeInterval = Constants.TIME_INTERVAL;
      this.makeListOfFiles();
    });

  }
  decorate() {
    this.cache.saveItem('time_interval', this.timeInterval).then(() => {
      this.makeListOfFiles();
    }).catch(() => {
      this.presentToast('error while saving time', 1000);
    });
  }
  makeListOfFiles() {
    this.cache.getItem('files_data')
      .then(
        data => {
          const fileMap = new Map();
          data.files.forEach(element => {
            fileMap.set(element.id, element);
          });

          this.fileMap = fileMap;

          console.log(this.user);
          console.log(this.fileMap);

          this.prepareFiles();
        },
        error => this.presentToast('error while retrieving files', 1000)
      );
  }

  async presentToast(messageText, timeout) {
    const toast = await this.toastController.create({
      message: messageText,
      duration: timeout
    });
    toast.present();
  }
  prepareFiles() {
    this.cache.getItem('saving_time').then(syncString => {
      console.log(syncString);
      this.syncDate = new Date(syncString);
      this.files = [];

      try {
        this.fileMap.forEach((value: any, key: number) => {
          const fileDate = new Date(value.updated_at);
          // console.log(value.created_at);
          if ((this.syncDate.getTime() - fileDate.getTime()) / (1000 * 60 * 60 * 24) <= this.timeInterval) {
            console.log('included', fileDate.getDate(), this.syncDate.getDate());
            const file = new IFile(value.id, value.folder_id, value.name, value.path, value.size,
              value.mime, value.ext, value.created_at, value.updated_at);
            this.files.push(file);
          }
        });
        this.files.sort((a, b) => (a.time() < b.time()) ? 1 : -1);
      } catch {
        this.presentToast('Internal error. Login Again', 1000);
      }
    }).catch(error => {
      this.presentToast('Error Parsing Files. Login Again', 1000);
    });
  }
  download(data) {
    console.log(data.download_url);
    window.open(data.download_url, '_system');
  }
  async dclick(file) {
    const alert = await this.alertController.create({
      header: 'Download this file?',
      message: 'Tap <strong>Ok</strong> to Download',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.presentToast('Generating Download URL...', 1000);

            const httpHeaders = new HttpHeaders({
              Accept: 'application/json',
              'Content-Type': 'application/json'
            });
            const options = {
              headers: httpHeaders
            };
            const params = {
              api_key: Constants.API_KEY,
              api_token: this.user.apiToken
            };

            const apiKey = Constants.API_KEY;
            const apiToken = this.user.apiToken;

            const url = Constants.BASE_URL + 'api/user/files/' +
            Number(file.id).toString() + '/download?api_key=' + apiKey + '&api_token=' + apiToken;

            // console.log(url);

            this.httpClient.get(url, options).subscribe(data => {this.download(data); },
            err => {this.presentToast('error getting durl', 1000); });
          }
        }
      ]
    });

    await alert.present();
  }
}
