import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiedPage } from './modified.page';

describe('ModifiedPage', () => {
  let component: ModifiedPage;
  let fixture: ComponentFixture<ModifiedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
