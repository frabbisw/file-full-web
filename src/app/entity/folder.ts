export class IFolder {
    id: number;
    name: string;
    parentId: number;
    createdAt: string;
    updatedAt: string;

    constructor(id, name, parentId, createdAt, updatedAt) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}
