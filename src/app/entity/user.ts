export class User {
    email: string;
    fullName: string;
    apiToken: string;

    constructor(email, fullName, apiToken) {
        this.email = email;
        this.fullName = fullName;
        this.apiToken = apiToken;
    }
}
