export class IFile {
    id: number;
    folderId: number;
    size: number;
    name: string;
    path: string;
    mime: string;
    ext: string;
    createdAt: string;
    updatedAt: string;
    percentage: 0.0;
    progressHidden = true;
    totalTime: number;
    constructor(id, folderId, name, path, size, mime, ext, createdAt, updatedAt) {
        this.id = id;
        this.folderId = folderId;
        this.name = name;
        this.path = path;
        this.size = size;
        this.mime = mime;
        this.ext = ext;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
    setPercentage(percentage) {
        this.percentage = percentage;
    }
    getPercentage() {
        return this.percentage;
    }
    hide() {
        this.progressHidden = true;
    }
    show() {
        this.progressHidden = false;
    }
    time() {
        return new Date(this.updatedAt).getTime();
    }
}
