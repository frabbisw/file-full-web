import { User } from '../entity/user';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserHolderService {
  user: User;
  // fileData: any;
  fileMap: Map<any, any>;
  folderMap: Map<any, any>;

  constructor() { }
  public setUser(user: User) {
    this.user = user;
  }

  public getUser() {
    return this.user;
  }

  public setFileMap(fileMap: Map<any, any>) {
    this.fileMap = fileMap;
  }

  public getFileMap() {
    return this.fileMap;
  }

  public setFolderMap(folderMap: Map<any, any>) {
    this.folderMap = folderMap;
  }

  public getFolderMap() {
    return this.folderMap;
  }
}
