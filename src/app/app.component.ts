import { Component } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { CacheService } from 'ionic-cache';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public alertController: AlertController,
    cache: CacheService
  ) {
    this.initializeApp();
    cache.setDefaultTTL(60 * 60 * 24 * 7);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    if (this.platform.is('android')) {
      this.platform.backButton.subscribeWithPriority(1, () => {
        this.confirmExit();
      });
    }
  }
  async confirmExit() {
    const alert = await this.alertController.create({
      header: 'Exit?',
      message: 'Tap <strong>Confirm</strong> to Exit',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
        , {
          text: 'Confirm',
          handler: () => {
            console.log('Confirm Open');
            (navigator as any).app.exitApp();
          }
        }
      ]
    });

    await alert.present();
  }
}
